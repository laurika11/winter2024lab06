public class LuckyCardGameApp{
	public static void main(String[] args){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 1;
		System.out.println("Hi, welcome to the Card Game.");
		
		while(manager.getNumberOfCards() > 1 && totalPoints < 5){
			System.out.println("Round: " + round);
			System.out.println(manager);
			totalPoints = totalPoints + manager.calculatePoints();
			System.out.println("Your points after round " + round + ": " + totalPoints);
			System.out.println("*******************************");
			manager.dealCards();
			round++;
		}
		if(totalPoints < 5){
			System.out.println("You lost with: " + totalPoints + " points.");
		}
		else {
			System.out.println("You won with: " + totalPoints + " points.");
		}
	}
}