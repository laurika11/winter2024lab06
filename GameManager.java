public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//sets the field
	public GameManager(){
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard(); //draw a card and put it into that field
		this.playerCard = drawPile.drawTopCard(); //draw a card and put it into that field
	}
	
	//shows the center and player card
	public String toString(){
		String dashes = "-------------------------------"; // adds dashes for decoration
		return dashes + "\n" + "Center card: " + this.centerCard.toString() + "\nPlayer card: " + this.playerCard.toString() + "\n" + dashes;
	}
	
	//updates the center card and player card
	public void dealCards(){
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard(); //draw a card and updates it into that field
		drawPile.shuffle();
		this.playerCard = drawPile.drawTopCard(); //draw a card and updates it into that field
	}
	
	//returns int representing the number of card on the drawPile
	public int getNumberOfCards(){	
		return drawPile.length();
	}
	
	//compares the cards by values or suits
	public int calculatePoints(){
		if(playerCard.getValue().equals(centerCard.getValue())){
			return 4;
		}
		else if(playerCard.getSuit().equals(centerCard.getSuit())){
			return 2;
		}
		else {
			return -1;
		}
	}
}